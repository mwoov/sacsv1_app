# README

This is the backend model implementation for the videogame _**Space Academy: Combat Simulator V.1**_. Its primary function is to allow players to register theirs scores on the game and show them to others as leaderboards. This backend provides all the endpoints used _in-game_, so it serves as an API for every function related to _player score registration_.

## Dev tools

- For backend implementation:
  - `Ruby v2.4.1p111`
  - `Ruby on Rails 5.1.2`
- Hosting services:
  - [Heroku - Link to app's homepage]()

## Getting started

The app is currently running on a Heroku app. For local testing you should do the following:

1. Clone _this_ repository on your local computer.
2. Install the needed _gems_, ignoring the production-state ones: `bundle install --without production`
3. Migrate the database: `rails db:migrate`
4. Start the local server: `rails server`

For testing purposes you should run: `rails test` and check (and hope) that everything is doing just fine.