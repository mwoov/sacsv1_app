class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception

  # Sets a placeholder for the homepage
  def hello
  	render html: "Welcome to the Space Academy. May the knowledge be with you."
  end
end
